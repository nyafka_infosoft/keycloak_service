# Registry where you want store your Docker images
DOCKER_REGISTRY = gcr.io/${GCLOUD-PROJECT-ID}
PYTHONPATH = $(shell pwd)/keycloak_service
PORTS = 8080:8080
TAG = latest
PROJECT_NAME = keycloak_service
GCLOUD-PROJECT-ID = home-260209
ENV = dev
MEMORY_LIMIT = 50M
ENV_VARIABLES = $(shell ./utils/convert_env.py $(shell pwd)/.env)

activate: 
	pip install --user poetry
	poetry install

lock:
	poetry lock 

freez: lock
	poetry export -f requirements.txt > requirements.pip

# pre production
build: freez
	docker build -t ${DOCKER_REGISTRY}/${PROJECT_NAME}:${TAG} .

run:
	PYTHONPATH=${PYTHONPATH} poetry run uvicorn keycloak_service.asgi:app --reload

push: build
	docker push ${DOCKER_REGISTRY}/${PROJECT_NAME}:${TAG}
