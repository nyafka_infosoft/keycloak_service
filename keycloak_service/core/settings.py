import os
from pathlib import Path

from pydantic import BaseSettings, SecretStr, DirectoryPath

BASE_DIR = Path(__file__).parent.parent
UP_DIR = BASE_DIR.parent.parent

e = os.environ.get


def get_env_path():
    _path = (
        (BASE_DIR / ".env.local"),
        (BASE_DIR / ".env"),
        (BASE_DIR.parent / ".env.local"),
        (BASE_DIR.parent / ".env"),
    )
    for _p in _path:
        if _p.exists():
            return _p.resolve()


class AppSettings(BaseSettings):
    title: str = "Keycloak service"
    version: str = "0.0.1"
    description: str = ""
    docs_url: str = "/swagger"


class RunSettings(BaseSettings):
    reload: bool = bool(e("DEBUG"))
    debug: bool = bool(e("DEBUG"))
    host: str = e("HOST", "0.0.0.0")
    port: int = int(e("PORT", 8000))
    log_level: str = "debug"

    class Config:
        env_file = get_env_path()


class Settings(BaseSettings):
    HOST: str = "0.0.0.0"
    PORT: int = 8000
    LOG_LEVEL: str = "debug"
    RELOAD: bool = True
    DEBUG: bool = True
    SENTRY_DSN: str = None
    # DB_PASSWORD: SecretStr = "mongo"
    # DB_NAME: str = "mongo_db"
    # DB_DSN: str = f"mongodb://{DB_NAME}:{DB_PASSWORD}@localhost:27017/{DB_NAME}"
    DB_POOL_MIN_SIZE: int = 1
    DB_POOL_MAX_SIZE: int = 16
    DB_ECHO: bool = True
    DB_SSL: str = None
    DB_USE_CONNECTION_FOR_REQUEST: bool = True
    DB_RETRY_LIMIT: int = 1
    DB_RETRY_INTERVAL: int = 1
    TEMPLATES_DIR: DirectoryPath = UP_DIR / "templates"
    MEDIA_PATH: DirectoryPath = BASE_DIR / "media"
    STATIC_PATH: DirectoryPath = BASE_DIR / "static"
    SECRET_KEY: str = "919dff2ce5d30bbf5690e3c0b2a578be3a4cd8361e6866ca2624ced99bbfc1bd"

    KEYCKLOAK_SERVER_URL: str = 'http://localhost:8080/auth/'
    KEYCKLOAK_CLIENT_ID: str = 'realm'
    KEYCKLOAK_REALM_NAME: str = 'demo'
    KEYCKLOAK_CLIENT_SECRET: SecretStr = '2889e139-100e-4a91-9353-1c204080a36c'

    KEYCKLOAK_ADMIN_USER: str = 'nyafka'
    KEYCKLOAK_ADMIN_PASSWORD: str = 'pass'
    KEYCKLOAK_ADMIN_CLIENT_ID: str = 'master'


    class Config:
        env_file = get_env_path()


settings = Settings()
