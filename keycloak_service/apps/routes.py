from fastapi import APIRouter

from apps.auth.views import v1

applications = APIRouter()

applications.include_router(v1, prefix="/auth/v1", tags=["auth"])
