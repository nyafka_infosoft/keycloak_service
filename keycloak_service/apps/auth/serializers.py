from pydantic import BaseModel, EmailStr, Field
from typing import List, Optional


class UserCreate(BaseModel):
    email: Optional[EmailStr] = Field(..., title='email пользователя')
    username: Optional[str] = Field(..., title='Логин пользователя')
    enabled = True
    firstName: Optional[str] = Field(..., title='Имя')
    lastName: Optional[str] = Field(..., title='Фамилия')
    realmRoles = ["user_default", ],
    attributes = {"example": "1,2,3,3,"}