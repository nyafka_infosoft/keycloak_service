import logging

from fastapi import APIRouter, Depends
from keycloak import KeycloakOpenID, KeycloakAdmin

from core.settings import settings

from .serializers import UserCreate

logger = logging.getLogger(__name__)
logger.setLevel("DEBUG")

v1 = APIRouter()

keycloak_openid = KeycloakOpenID(
    server_url=settings.KEYCKLOAK_SERVER_URL,
    client_id=settings.KEYCKLOAK_CLIENT_ID,
    realm_name=settings.KEYCKLOAK_REALM_NAME,
    client_secret_key=settings.KEYCKLOAK_CLIENT_SECRET
)

keycloak_admin = KeycloakAdmin(
    server_url=settings.KEYCKLOAK_SERVER_URL,
    username=settings.KEYCKLOAK_ADMIN_USER,
    password=settings.KEYCKLOAK_ADMIN_PASSWORD,
    realm_name=settings.KEYCKLOAK_ADMIN_CLIENT_ID,
    verify=True
)


@v1.get("/get_token")
async def auth(user: str, password: str):
    token = keycloak_openid.token(user, password)

    return token['access_token']


@v1.get("/user_info")
async def auth(token: str):

    return keycloak_openid.userinfo(token)


@v1.get("/create_user")
async def create_user(form_data:  UserCreate = Depends()):
    return keycloak_admin.create_user(form_data)